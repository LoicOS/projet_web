<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:data.sqlite3');
});

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('trad', function($string){
        return $string;
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    Flight::render('index.twig');
});

Flight::route('/phones', function(){

    $data=[
        'telephones' => Model::factory('Telephone')->find_many(),
    ];
Flight::render('phones.twig', $data);
});

Flight::route('/phone/@mod', function($mod){
    $data=[
        'phone' =>Model::factory('Telephone')->where('modele', $mod)->find_one(),
    ];
Flight::render('phone.twig', $data);
});




Flight::route('/test', function(){
    $marques = Model::factory('Marque')->find_many();
    $data = [
        'marques' => $marques,
        'sony' => Model::factory('Marque')->where('nom', 'Sony')->find_one(),
        'telephones' => Model::factory('Telephone')->find_many(),
    ];
    Flight::render('test.twig', $data);
});


Flight::start();

?>