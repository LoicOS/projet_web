<?php
function recupTelephone($database) {
    $sql = 'SELECT id , marque.nom AS Marque,
            modele As Modèle, prix as Prix, image
            FROM telephone,marque
            WHERE telephone.idmarque = marque.id';
    return query($database, $sql);
}


function compterTelephone($database) {
    $sql = 'SELECT count(id) AS Total FROM telephone';
    return queryOne($database, $sql);
}


function recupTelephoneCarac($database){
    $sql = 'SELECT telephone.id, modele as Modèle,prix As Prix,image as Image,taille AS Taille,
    autonomie as Autonomie,qualitephoto As Qualité photo,couleur AS Couleur,capacite As Capacité,';
    return queryOne($database, $sql);
}

function creerTelephone($database, $id, $idmarque, $modele, $prix, $image, $taille, $autonomie, $qualitephoto, $couleur, $capacite) {
    $sql = 'INSERT INTO telephone (id, idmarque, modele,prix,image,taille,autonomie,qualitephoto,couleur,capacite)
            VALUES (:id, :idmarque, :modele, :prix, :image, :taille, :autonomie, :qualitephoto, :couleur, :capacite)';
    $parameters = array(
        'id' => $id,
        'idmarque' => $idmarque,
        'modele' => $modele,
        'prix' => $prix,
        'image' => $image,
        'taille' => $taille,
        'autonomie' => $autonomie,
        'qualitephoto' => $qualitephoto,
        'couleur' => $couleur,
        'capacite' => $capacite

    );
    queryNoResult($database, $sql, $parameters);
}




