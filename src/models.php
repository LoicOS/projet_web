<?php

class Marque extends Model
{
    public function telephones()
    {
        return $this->has_many('Telephone', 'idmarque')->find_many();
    }
}

class Telephone extends Model
{
    public function marque()
    {
        return $this->belongs_to('Marque', 'idmarque')->find_one();
    }

    
}